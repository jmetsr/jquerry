$.Tabs = function (el) {
  this.$el = el;
  this.$contentTabs = this.$el.data-content-tabs;
  this.$active = this.$el.active
 };

$.fn.tabs = function () {
  return this.each(function () {
    new $.Tabs(this);
  });


};